CREATE DATABASE emotion;

CREATE TABLE `em_messages` (
  `id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `fullName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `email` varchar(320) NOT NULL,
  `message` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `em_messages` (`id`, `createdAt`, `fullName`, `firstName`, `lastName`, `dateOfBirth`, `email`, `message`) VALUES
(44, '2020-12-06 23:01:00', 'Vardas Pavardė', '', '', '1995-03-28', 'example@example.com', 'Įkėlėme šeimos dienos akciją. Dėl papildomos medžiagos užtrukome šiek tiek ilgiau nei įprasta.'),
(45, '2020-12-06 23:05:01', 'Vardas Pavardė', '', '', '1995-03-28', '', 'Įkėlėme šeimos dienos akciją. Dėl papildomos medžiagos užtrukome šiek tiek ilgiau nei įprasta.\r\n'),
(46, '2020-12-06 23:07:48', 'Vardas Pavardė', '', '', '1995-03-28', '', 'Įkėlėme šeimos dienos akciją. Dėl papildomos medžiagos užtrukome šiek tiek ilgiau nei įprasta.\r\n');

ALTER TABLE `em_messages`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `em_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;