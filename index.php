<?php

require "Db.php";
require "Messages.php";
if (!isset($_GET['page'])) {
    $page = 0;
} else {
    $page = $_GET['page'];
}
$messageModel = new Messages();
$messages = $messageModel->fetchAll($page);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Žinutės</title>
    <link rel="stylesheet" media="screen" type="text/css" href="css/screen.css"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
<div id="wrapper">
    <h1>Jūsų žinutės</h1>
    <form id="form" action="addMessage.php" method="post">
        <p>
            <label for="fullName">Vardas, pavardė *</label><br/>
            <input id="fullName" type="text" name="fullName" value=""/>
        </p>
        <p>
            <label for="dateOfBirth">Gimimo data *</label><br/>
            <input id="dateOfBirth" type="text" name="dateOfBirth" value=""/>
        </p>
        <p>
            <label for="email">El.pašto adresas</label><br/>
            <input id="email" type="text" name="email" value=""/>
        </p>
        <p> <!--class="err"-->
            <label for="message">Jūsų žinutė *</label><br/>
            <textarea id="message" name="message" cols="2" rows="2"></textarea>
        </p>
        <p>
            <span>* - privalomi laukai</span>
            <input type="submit" value="Skelbti"/>
            <img id="loader" class="hidden" src="img/ajax-loader.gif" alt=""/>
        </p>
    </form>
    <ul>
        <?php if (count($messages['data']) > 0) { ?>
            <?php foreach ($messages['data'] as $message) { ?>
                <li>
                    <span><?= $message['createdAt'] ?></span>
                    <?= $message['email'] != '' ? '<a href="mailto:' . $message['email'] . '">' . $message['fullName'] . '</a>' : $message['fullName'] ?>
                    ,<?= $message['dateOfBirth'] ?> m. <br/>
                    <?= $message['message'] ?>
                </li>
            <?php } ?>
        <?php } else { ?>
            <li>
                <strong>Šiuo metu žinučių nėra. Būk pirmas!</strong>
            </li>
        <?php } ?>
    </ul>
    <p class="<?= $messages['pages'] == 0 ? 'hidden' : '' ?>" id="pages" currentPage="<?= $page ?>">
        <?php for ($page = 0; $page <= $messages['pages']; $page++): ?>
            <a href='<?php echo "?page=$page"; ?>' title="<?= $page ?>" class="links"><?php echo $page; ?>
            </a>
        <?php endfor; ?>
    </p>
</div>
</body>
</html>
