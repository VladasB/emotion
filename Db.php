<?php
ini_set('error_reporting', E_STRICT);

class Db
{
    private $dbc = null;
    private $stmt = null;

    private $host = 'localhost';
    private $user = 'root';
    private $pass = '';
    private $name = 'emotion';

    public $errors = [];

    const MAX_ITEMS_PER_REQUEST = 5;

    function __construct()
    {
        try {
            $this->dbc = new PDO(
                "mysql:host={$this->host};
    dbname={$this->name};charset=utf8",
                $this->user, $this->pass, [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES => false,
                ]
            );
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    function __destruct()
    {
        if ($this->stmt !== null) {
            $this->stmt = null;
        }
        if ($this->dbc !== null) {
            $this->dbc = null;
        }
    }

    function fetchAll($tableName, $page, $limit = self::MAX_ITEMS_PER_REQUEST, $order = 'DESC')
    {
        if (!is_int(intval($page))) {
            die('page is not a number');
        }
        $sql = 'SELECT * FROM ' . $tableName;
        $offset = $page * $limit;
        try {
            $this->stmt = $this->dbc->prepare($sql);
            $this->stmt->bindParam("tableName", $tableName);
            $this->stmt->execute();
            $count = $this->stmt->rowCount();
            $sql .= ' ORDER BY id ' . $order;
            $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;
            $this->stmt = $this->dbc->prepare($sql);
            $this->stmt->execute();
            $result = $this->stmt->fetchAll();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        $this->stmt = null;
        return [
            'data' => $result,
            'page' => $page,
            'pages' => ceil($count / $limit) - 1
        ];
    }

    function save($model, $table)
    {
        $sql = $this->buildSaveSql($table, $model);
        try {
            $this->stmt = $this->dbc->prepare($sql);
            $this->stmt->bindParam("table", $table);
            foreach ($model as $key => $column) {
                $this->stmt->bindParam($key, $column);
            }
            $result = $this->stmt->execute();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        $this->stmt = null;
        return $result ? $result : false;

    }

    public function buildSaveSql($table, $model)
    {
        $sql = 'INSERT INTO ' . $table . ' (';
        foreach ($model as $key => $column) {
            $sql .= '' . $key . ', ';
        }
        $sql = substr($sql, 0, -2);
        $sql .= ') VALUES (';
        foreach ($model as $key => $column) {
            $sql .= '"' . $column . '", ';
        }
        $sql = substr($sql, 0, -2);
        $sql .= ')';
        return $sql;
    }

    function validate($model, $rules)
    {
        foreach ($rules as $rule) {
            $attr = $rule[0];
            $ruleName = $rule[1];
            switch ($ruleName) {
                case 'required':
                    $this->validateRequired($model->$attr, $attr);
                    break;
                case 'validateFirstLastNames':
                    $this->validateFirstLastNames($model->$attr, $attr);
                    break;
                case 'stringPurify':
                    $this->validateCleanString($model->$attr, $attr);
                    break;
                case 'email':
                    $this->validateEmail($model->$attr, $attr);
                    break;
                case 'date':
                    $this->validateDate($model->$attr, $attr);
                    break;
                default:
                    print_r('no rule');
            }
        }
        if (empty($this->errors)) {
            return true;
        } else {
            return false;
        }
    }

    public function validateFirstLastNames($fullName, $attrName)
    {
        $data = explode(' ', $fullName);
        if (count($data) >= 2) {
            return true;
        } else {
            $this->addErrors($attrName, 'validateFirstLastNames', 'not 2 words');
            return false;
        }
    }

    public function validateCleanString($string, $attrName)
    {
        $string = strip_tags($string);
        $string = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $string);
        if ($string) {
            return true;
        } else {
            $this->addErrors($attrName, 'validateCleanString', 'no string left');
            return false;
        }
    }

    public function validateEmail($email, $attrName)
    {

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return true;
        } else {
            $this->addErrors($attrName, 'validateEmail', 'bad email');
            return false;
        }
    }

    public function validateRequired($attr, $attrName)
    {

        if ($attr != '') {
            return true;
        } else {
            $this->addErrors($attrName, 'validateRequired', 'field is required');
            return false;
        }
    }

    public function validateDate($attr, $attrName)
    {

//        if ($attr != '') {
            return true;
//        } else {
//            $this->addErrors($attrName, 'validateDate', 'date not correctly formated yyyy-mm-dd (1995-03-28)');
//            return false;
//        }
    }

    public function addErrors($attrName, $rule, $message)
    {
        $this->errors[] = [
            'attrName' => $attrName,
            'rule' => $rule,
            'message' => $message,
        ];
    }
}

