<?php
require_once 'Db.php';

/**
 * This is the model class for table "em_messages".
 *
 * The followings are the available columns in table 'em_messages':
 * @property integer $id
 * @property integer $created_at
 * @property string $name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $email
 * @property string $message
 *
 * @property string $createdAt
 * @property string $lastName
 * @property string $dateOfBirth
 *
 */
class Messages
{

    public $model;
    public $db;

    function __construct()
    {
        $this->db = new Db();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'em_messages';
    }

    public function rules()
    {
        return [
            ['fullName', 'validateFirstLastNames'],
            ['fullName', 'stringPurify'],
            ['fullName', 'required'],
            ['dateOfBirth', 'date'],
            ['email', 'email'],
            ['message', 'stringPurify'],
            ['message', 'required'],
        ];
    }

    public function set($fullName, $dateOfBirth, $email, $message)
    {
        $this->setFullName($fullName);
        $this->setDateOfBirth($dateOfBirth);
        $this->setEmail($email);
        $this->setMessage($message);
    }

    public function validate()
    {
        return $this->db->validate($this->model, $this->rules());
    }

    public function fetchAll($page)
    {
        return $this->db->fetchAll($this->tableName(), $page);
    }

    public function addMessage()
    {
        return $this->db->save($this->model, $this->tableName());
    }

    public function getFullName()
    {
        return $this->model->fullName;
    }

    public function setFullName($value)
    {
        $this->model->fullName = $value;
    }

    public function getCreatedAt()
    {
        return $this->model->createdAt;
    }

    public function getDateOfBirth()
    {
        return $this->model->dateOfBirth;
    }

    public function setDateOfBirth($value)
    {
        $this->model->dateOfBirth = $value;
    }

    public function getEmail()
    {
        return $this->model->email;
    }

    public function setEmail($value)
    {
        $this->model->email = $value;
    }

    public function getMessage()
    {
        return $this->model->message;
    }

    public function setMessage($value)
    {
        $this->model->message = $value;
    }
}
