<?php
require_once 'Messages.php';
$messageModel = new Messages();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_POST) {
        $messageModel->set($_POST['fullName'], $_POST['dateOfBirth'], $_POST['email'], $_POST['message']);
        if (!$messageModel->validate()) {
            die(json_encode(['resultMessage' => 'validationFail', 'errors' => $messageModel->db->errors]));
        }
        if (!$messageModel->addMessage()) {
            die('failed to add message');
        }
        die(json_encode(['resultMessage' => 'success']));
    } else {
        die('empty');
    }
} else {
    die('Only GET');
}
