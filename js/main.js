$(document).ready(function () {
    $('#loader').addClass('hidden');
    $("#form").submit(function (event) {
        $("#form").children('p').addClass('read-only').removeClass('err');
        $('#loader').removeClass('hidden');
        event.preventDefault();
        var url = $(this).attr('action');
        $.ajax({
            type: "post",
            url: url,
            data: $(this).serialize(),
        }).done(function (json) {
            $('#loader').addClass('hidden');
            $("#form").children('p').removeClass('read-only');
            var response = $.parseJSON(json);
            if (response.resultMessage === 'validationFail') {
                $.each(response.errors, function (i, error) {
                    $('#' + error.attrName).parent().addClass('err')
                });
            } else {
                window.location.replace("/emotion/");
            }
        });
    });

});

